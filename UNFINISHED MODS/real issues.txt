Real Issues:

Party Rigidity:
Overwhelming (-0.4)
Established (-0.2)
Loose (0.08)
Tenous (0.15)
Open to Reform (0.31)

Complacency: 
Complacent (-0.55)
Unaffected (0)
Uneasy (0.25)
Discontented (0.4)

Faith:
Unquestioned (-0.45)
Trusting (-0.2)
Questioning (-0.1)
Feels Flaws (0.25)
Disillusioned (0.42)

Downballot:
Safe R. (-0.333)
Lean R. (-0.15)
Tossup (0)
Lean D. (0.25)
Safe D. (0.4)

Primary Winner:
Mondale (-0.55)
Hart (-0.2)
Jackson (-0.1)
Glenn (0.3)
McGovern (0.4)
Uncommitted (-0.8)