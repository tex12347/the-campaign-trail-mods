document.getElementById("game_start").click();
document.getElementById("election_id").value = 13;
document.getElementById("election_id_button").click();
document.getElementById("candidate_id").value = 132;
document.getElementById("candidate_id_button").click();
document.getElementById("running_mate_id").value = 139;
document.getElementById("running_mate_id_button").click();
document.getElementById("difficulty_level_id").value = 5;
document.getElementById("opponent_selection_id_button").click();

let answersToClick = [4155, 4158, 4160, 4192, 4223, 4199, 4188, 4195, 4186, 4170, 4174, 4183, 4185, 4178, 4206, 4181, 4172, 4207, 4213, 4232, 4204, 4225, 4228, 4221, 4216, 4211, 4233];

function autoplay() {
    try {
        const confirm = document.getElementById("confirm_visit_button");
    
        if(confirm) confirm.click();

        const questionLength = document.getElementById("question_form").children[0].children.length / 3;

        for (let i = 0; i < questionLength; i++) {
            res = checkIfAnswer(i);
        }

        campaignTrail_temp.election_json[0].fields.has_visits = false;
    }
    catch {

    }
}

function checkIfAnswer(i) {
    const object = document.getElementById("question_form").children[0].children[(i * 3)]
    const pk = Number(object.value);
    if(answerSet.has(pk)) {
        object.click();
        document.getElementById("answer_select_button").click();
        document.getElementById("ok_button").click();
    }
}

const answerSet = new Set(answersToClick);
setInterval(autoplay, 10);